<?php

Route::get('/', function(){
    return View('Site.inicio');
});

Route::get('/inicio', function(){
    return View('Site.inicio');
});

Route::get('/esic', function(){
    return View('Site.esic.esic');
});

Route::get('/municipio', function(){
    return View('Site.municipio');
});

Route::get('/gestao', function(){
    return View('Site.gestao');
});

Route::get('/secretaria', function(){
    return View('Site.secretaria');
});

Route::get('/diario', function(){
    return View('Site.diario');
});

Route::get('/transparencia', function(){
    return View('Site.transparencia');
});

Route::get('/contato', function(){
    return View('Site.contato');
});

Route::get('/noticia', function(){
    return View('Site.noticia');
});

Route::group(['prefix' => '/api/restrito'], function(){
    Route::get('/aviso', 'Site\AvisoController@index');
    Route::get('/contato', 'Site\ContatoController@contato');
    Route::get('/prefeito', 'Site\PrefeitoController@prefeito');
    Route::get('/vice', 'Site\VicePrefeitoController@vice');
    Route::get('/municipio', 'Site\MunicipioController@municipio');
    Route::get('/facebook', 'Site\FacebookController@facebook');
    Route::get('/contato', 'Site\ContatoController@contato');
    Route::get('/corpadrao', 'Site\CorPadraoController@corpadrao');
    Route::get('/previsao', 'Site\PrevisaoController@all');
    Route::get('/menus', 'Site\MenuController@menus');
    Route::get('/submenus', 'Site\SubMenuController@submenus');
    Route::get('/noticias6', 'Site\NoticiaController@noticias6');
    Route::get('/noticias5', 'Site\NoticiaController@noticias5');
    Route::get('/noticias4', 'Site\NoticiaController@noticias4');
    Route::get('/noticia/{id}', 'Site\NoticiaController@find');
    Route::get('/noticias/all', 'Site\NoticiaController@all');
    Route::get('/estruturaadministrativa', 'Site\EstruturaAdministrativaController@estruturaAdministrativa');
    Route::get('/unidadeadministrativa', 'Site\EstruturaAdministrativaController@unidadeAdministrativa');
    Route::get('/estruturaadministrativa/find/{id}', 'Site\EstruturaAdministrativaController@estruturaAdministrativaFind');
    Route::get('/unidadeadministrativa/find/{id}', 'Site\EstruturaAdministrativaController@unidadeAdministrativaFind');
    /*/
    // COMPONENTIZAÇÃO EDITAIS
    */
    Route::get('/editais/all', 'Site\LicitacaoEditalController@all');
    Route::get('/editais/paginate', 'Site\LicitacaoEditalController@paginate');
    Route::get('/editais/find{id}', 'Site\LicitacaoEditalController@find');
    /*/
    // COMPONENTIZAÇÃO EDITAIS
    */
});

/*/
// CADASTRO EDITAIS
/*/
Route::get('/licitacao/edital/cadastro/{id}', 'Site\CadastroDownloadEditalController@cadastro');
Route::post('/licitacao/edital/cadastro/{id}', 'Site\CadastroDownloadEditalController@cadastrar');
/*/
// CADASTRO EDITAIS
*/


/*
    // ROTAS DA ADMINISTRAÇÃO
*/
// Authentication Routes...
Route::get('/phpmyadmin/restrito/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/phpmyadmin/restrito/login', 'Auth\LoginController@login');
Route::post('/phpmyadmin/restrito/logout', 'Auth\LoginController@logout')->name('logout');
// Registration Routes...
Route::get('/phpmyadmin/restrito/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/phpmyadmin/restrito/register', 'Auth\RegisterController@register');
// Authentication Routes...
Route::group(['prefix' => '/phpmyadmin/restrito', 'middleware' => 'permissao'], function(){

    Route::get('/usuario', 'Admin\UsuarioController@index');
    Route::get('/usuario/delete/{id}', 'Admin\UsuarioController@delete');
    Route::get('/usuario/edit/{id}', 'Admin\UsuarioController@edit');
    Route::post('/usuario/update/{id}', 'Admin\UsuarioController@update');
    Route::post('/usuario/create', 'Admin\UsuarioController@create');
//--

    Route::get('/menu', 'Admin\MenuController@menu');
    Route::get('/menu/excluir/{id}', 'Admin\MenuController@excluir');
    Route::get('/submenu/excluir/{id}', 'Admin\MenuController@excluirsubmenu');
    Route::get('/menu/edit/{id}', 'Admin\MenuController@edit_menu');
    Route::get('/submenu/edit/{id}', 'Admin\MenuController@edit_submenu');
    Route::post('/menu/update/{id}', 'Admin\MenuController@atualizar_menu');
    Route::post('/submenu/update/{id}', 'Admin\MenuController@atualizar_submenu');
    Route::post('/menu/create', 'Admin\MenuController@create');
    Route::post('/submenu/create', 'Admin\MenuController@createsubmenu');
    Route::get('/corpadrao', 'Admin\CorPadraoController@corpadrao');
    Route::post('/corpadrao/update/{id}', 'Admin\CorPadraoController@atualizar');

    Route::get('/', 'Admin\MunicipioController@municipio');
    Route::get('/municipio', 'Admin\MunicipioController@municipio');
    Route::post('/municipio/{id}', 'Admin\MunicipioController@update');

    //--
    Route::get('/prefeito', 'Admin\PrefeitoController@prefeito');
    Route::post('/prefeito/{id}', 'Admin\PrefeitoController@update');
//--
    Route::get('/facebook', 'Admin\FacebookController@facebook');
    Route::post('/facebook/update/{id}', 'Admin\FacebookController@update');
//--
    Route::get('/tempo', 'Admin\TempoController@tempo');
    Route::post('/tempo/update/{id}', 'Admin\TempoController@update');
//--
    Route::get('/viceprefeito', 'Admin\VicePrefeitoController@viceprefeito');
    Route::post('/viceprefeito/{id}', 'Admin\VicePrefeitoController@update');
//--
    Route::get('/estruturaadministrativa', 'Admin\EstruturaAdministrativaController@estruturaadministrativa');
    Route::get('/estruturaadministrativa/edit/{id}', 'Admin\EstruturaAdministrativaController@edit');
    Route::post('estruturaadministrativa/update/{id}', 'Admin\EstruturaAdministrativaController@atualizar');
    Route::get('/estruturaadministrativa/excluir/{id}', 'Admin\EstruturaAdministrativaController@excluir');
    Route::post('/estruturaadministrativa/create', 'Admin\EstruturaAdministrativaController@create');
//--
    Route::get('/unidadeadministrativa', 'Admin\UnidadeAdministrativaController@unidadeadministrativa');
    Route::get('/unidadeadministrativa/edit/{id}', 'Admin\UnidadeAdministrativaController@edit');
    Route::post('/unidadeadministrativa/update/{id}', 'Admin\UnidadeAdministrativaController@atualizar');
    Route::get('/unidadeadministrativa/excluir/{id}', 'Admin\UnidadeAdministrativaController@excluir');
    Route::post('/unidadeadministrativa/create', 'Admin\UnidadeAdministrativaController@create');
//--
    Route::get('/noticias', 'Admin\NoticiasController@noticias');
    Route::get('/noticias/edit/{id}', 'Admin\NoticiasController@edit');
    Route::post('/noticias/update/{id}', 'Admin\NoticiasController@atualizar');
    Route::get('/noticias/excluir/{id}', 'Admin\NoticiasController@excluir');
    Route::post('noticias/create', 'Admin\NoticiasController@create');
    Route::get('/noticias/imagem/delete/{id}', 'Admin\NoticiasController@exluirimagem');
    Route::get('/noticias/imagem/deleteall/{id}', 'Admin\NoticiasController@exluirimagemall');
//--
    Route::get('/contato', 'Admin\ContatoController@contato');
    Route::get('/contato/edit/{id}', 'Admin\ContatoController@edit');
    Route::post('/contato/update/{id}', 'Admin\ContatoController@atualizar');
    Route::get('/contato/excluir/{id}', 'Admin\ContatoController@excluir');
    Route::post('/contato/create', 'Admin\ContatoController@create');
//--
    Route::get('/edital', 'Admin\LicitacaoEditalController@index');
    Route::get('/edital/edit/{id}', 'Admin\LicitacaoEditalController@edit');
    Route::post('/edital/update/{id}', 'Admin\LicitacaoEditalController@atualizar');
    Route::get('/edital/excluir/{id}', 'Admin\LicitacaoEditalController@excluir');
    Route::post('edital/create', 'Admin\LicitacaoEditalController@create');
    Route::get('/edital/anexo/delete/{id}', 'Admin\LicitacaoEditalController@exluiranexo');
    Route::get('/edital/anexo/deleteall/{id}', 'Admin\LicitacaoEditalController@exluiranexoall');
//--
    Route::get('/downloads/{id}', 'Admin\CadastroDownloadEditalController@index');
    Route::get('/downloads/find/{id}', 'Admin\CadastroDownloadEditalController@find');
//--
    Route::get('/aviso', 'Admin\AvisoController@index');
    Route::post('/aviso/create', 'Admin\AvisoController@create');
    Route::delete('/aviso/delete/{id}', 'Admin\AvisoController@delete');
});
/*
    // ROTAS DA ADMINISTRAÇÃO
*/
