<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class EsicUser extends Authenticatable
{
    protected $table = "esic_users";

    protected $fillable = [
        'name', 'email', 'password', 'tipo', 'cpf_cnpj'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
    protected $hidden = [
        'password', 'remember_token',
    ];
}
