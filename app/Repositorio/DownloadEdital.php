<?php

namespace App\Repositorio;

use Illuminate\Database\Eloquent\Model;

class DownloadEdital extends Model
{
    protected $table = "download_editais";

    protected $fillable = [
        'id_licitacao', 'id_cadastro_download'
    ];
}