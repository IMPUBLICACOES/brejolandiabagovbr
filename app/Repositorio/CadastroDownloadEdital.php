<?php

namespace App\Repositorio;

use Illuminate\Database\Eloquent\Model;
use DB;

class CadastroDownloadEdital extends Model
{
    protected $table = "cadastro_download_editais";

    
    protected $fillable = [
        'fornecedor', 'id_licitacao', 'cnpj', 'telefone', 'email', 'endereco', 'cidade', 'estado', 'cep',
        'nome_representante', 'rg_representante', 'orgao_exp_rg_representante', 'cpf_representante', 'created_at'
    ];
    
    protected $appends = ['data_convertida', 'licitacacao'];

    public function getDataConvertidaAttribute()
    {
        return date('d/m/Y', strtotime($this->attributes['created_at']));                                                          
    }

    public function getLicitacaoAttribute()
    {
        return DB::table('cadastro_download_editais')
               ->join('licitacacoes', 'cadastro_download_editais.id_licitacao', '=', 'licitacacoes.id')
               ->where('licitacacoes.id', '=', $this->attributes['id_licitacao'])
               ->select('licitacacoes.moalidade')
               ->get();
    }

    protected function novo($request, $licitacao){
        $result = DB::table('cadastro_download_editais')
                    ->insertGetId([
                    'fornecedor' => $request->input('fornecedor'),
                    'cnpj' => $request->input('cnpj'),
                    'telefone' => $request->input('telefone'),                                                               
                    'email' => $request->input('email'),
                    'endereco' => $request->input('endereco'),
                    'cidade' => $request->input('cidade'),
                    'estado' => $request->input('estado'),
                    'email' =>$request->input('email'),
                    'cep' =>$request->input('cep'),
                    'nome_representante' => $request->input('nome_representante'),
                    'rg_representante' => $request->input('rg_representante'),
                    'orgao_exp_rg_representante' => $request->input('orgao_exp_rg_representante'),
                    'cpf_representante' => $request->input('cpf_representante'),
                    'id_licitacao' => $licitacao->id,
                ]); 
        return $result;
    }

    protected function excluirAll($id){
        return  DB::table('cadastro_download_editais')
                ->where('id_licitacao', '=', $id)
                ->delete();
    }

    protected function findAllById($id){
        return  DB::table('cadastro_download_editais')
                ->where('id_licitacao', '=', $id)
                ->get();
    }
}