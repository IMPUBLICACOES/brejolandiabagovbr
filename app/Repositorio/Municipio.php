<?php

namespace App\Repositorio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use DB;
use File;

class Municipio extends Model
{
    protected $table = "municipios";

    protected $fillable = [
        'nome', 'descricao', 'texto', 'url_imagem'
    ];

    protected $appends = ['texto_resumido'];

    public function gettextoResumidoAttribute()
    {
        return str_limit($this->getAttribute('texto'), 90, '...');
    }

    protected static function atualizar($request, $id, $file){        
        if($file == null || $file == ''){
            $atualizar = DB::table('municipios')->where('id', $id)
                ->update([
                'nome' => $request->input('nome'),
                'descricao' => $request->input('descricao'),
                'texto' => $request->input('texto'),                
            ]);
        } else if($file){
            $municipio = Municipio::findById($id);
            $url_antiga = "";
            foreach($municipio as $m){
                if($m->url_imagem != '' || $m->url_imagem != null){
                    $url_antiga = $m->url_imagem;
                }
            }            
            $atualizar = DB::table('municipios')->where('id', $id)
            ->update([
                'nome' => $request->input('nome'),
                'descricao' => $request->input('descricao'),
                'texto' => $request->input('texto'),                
                'url_imagem' => $file,
            ]);            
            if($atualizar != 0  && $url_antiga != "" && $url_antiga != null){                
                if(File::exists('storage/' . $url_antiga)){                    
                    File::delete('storage/' . $url_antiga); //DELETA O ARQUIVO ANTIGO
                }
            }                        
        }          
        return $atualizar;
    }

    protected static function findById($id){
        $municipio = DB::table('municipios')
                    ->where('id', '=', $id)
                    ->get();
        return $municipio;
    }
}