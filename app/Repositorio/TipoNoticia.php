<?php

namespace App\Repositorio;

use Illuminate\Database\Eloquent\Model;

class TipoNoticia extends Model
{
    protected $table = "tipo_noticias";

    protected $fillable = [
        'nome'
    ];
}
