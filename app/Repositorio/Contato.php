<?php

namespace App\Repositorio;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
    protected $table = 'contatos';
    
    protected $fillable = [
        'nome', 'numero'
    ];
}
