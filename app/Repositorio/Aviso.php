<?php

namespace App\Repositorio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Carbon;
use File;
use DB;

class Aviso extends Model
{
    protected $table = "avisos";

    protected $fillable = [
        'id', 'titulo', 'url_imagem', 'texto'
    ];

    protected static function criar($request){
        $file = null;

        if($request->file('url_imagem')){
            $file =  $request->file('url_imagem')->store('avisos', 'public');
        }

        $salvo = DB::table('avisos')
        ->insertGetId([
            'texto' => $request->input('texto'),
            'titulo' => $request->input('titulo'),
            'url_imagem' => $file
        ]);

        return $salvo;
    }
}
