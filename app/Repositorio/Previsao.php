<?php

namespace App\Repositorio;

use Illuminate\Database\Eloquent\Model;

class Previsao extends Model
{    
    protected $table = "tempo";

    protected $fillable = [
        'codigo_municipio', 'chave_sistema'
    ];

    //https://api.hgbrasil.com/weather?array_limit=2&fields=only_results,temp,city_name,forecast,max,min,date&key=SUA-CHAVE
    //return json_decode(file_get_contents('https://api.hgbrasil.com/weather?key=' . $chave . '&woeid=' . $codigo . '&format=json'), true);

    protected function contents(){
        $tempo = $this->all();
        foreach($tempo as $t){
            $codigo = $t->codigo_municipio;
            $chave = $t->chave_sistema;
        }        
        return json_decode(file_get_contents('https://api.hgbrasil.com/weather?key=' . $chave . '&woeid=' . $codigo . '&format=json'), true);        
    }
}