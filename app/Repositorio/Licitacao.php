<?php

namespace App\Repositorio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Repositorio\LicitacaoAnexo;
use Illuminate\Support\Carbon;
use DB;
use File;

class Licitacao extends Model
{
    protected $table = "licitacacoes";

    protected $fillable = [
        'moalidade', 'objeto', 'data',
    ];

    protected $appends = ['data_convertida', 'url_anexos', 'mes_licitacao', 'ano_licitacao', 'ano_atual'];

    public function getDataConvertidaAttribute()
    {
        return date('d/m/Y', strtotime($this->attributes['data']));
    }

    public function getUrlAnexosAttribute()
    {
        return DB::table('licitacacoes')
               ->join('licitacao_anexos', 'licitacao_anexos.id_licitacao', '=', 'licitacacoes.id')
               ->where('licitacacoes.id', '=', $this->attributes['id'])
               ->select('licitacao_anexos.id', 'licitacao_anexos.url_anexo')
               ->get();
    }

    public function getMesLicitacaoAttribute()
    {
        return date('m', strtotime($this->attributes['data']));
    }

    public function getAnoLicitacaoAttribute()
    {
        return date('Y', strtotime($this->attributes['data']));
    }

    public function getAnoAtualAttribute()
    {
        $atual = Carbon::now();
        return strval($atual->year);
    }

    protected static function criar($request, $file){
        $year = date('Y', strtotime($request->input('data_edital')));
        $month = date('m', strtotime($request->input('data_edital')));
        $data = '/' .$year . '/' . $month;

        $id = DB::table('licitacacoes')
        ->insertGetId([
            'modalidade' => $request->input('modalidade'),
            'objeto' => $request->input('objeto'),
            'data' => $request->input('data_edital'),
        ]);

        if($file){
            if($data){
                if($id){
                    LicitacaoAnexo::criar($id, $file, $data);
                }
            }
        }
        return $id;
    }

    protected static function atualizar($request, $id, $file){
        $year = date('Y', strtotime($request->input('data_edital')));
        $month = date('m', strtotime($request->input('data_edital')));
        $data = '/' .$year . '/' . $month;
        
        $atualizar = DB::table('licitacacoes')
        ->where('id', $id)->update([
            'modalidade' => $request->input('modalidade'),
            'objeto' => $request->input('objeto'),
            'data' => $request->input('data_edital'),
        ]);
           
        if($file){
            if($id){                
                LicitacaoAnexo::criar($id, $file, $data);        
            }
        }
        return $atualizar;
    }    
}
