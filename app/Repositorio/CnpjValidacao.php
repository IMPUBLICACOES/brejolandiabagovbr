<?php

namespace App\Repositorio;

use Illuminate\Database\Eloquent\Model;

class CnpjValidacao extends Model
{
    protected function buscar($cnpj){        
        return  json_decode(file_get_contents('https://www.receitaws.com.br/v1/cnpj/'. $cnpj), true);
    }    
}