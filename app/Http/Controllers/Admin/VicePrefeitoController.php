<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositorio\VicePrefeito;

class VicePrefeitoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function viceprefeito(){
        $viceprefeito = VicePrefeito::all();
        return View('Admin.VicePrefeito.viceprefeito', compact('viceprefeito'));
    }

    protected function update(Request $request, $id){
        $file ='';
        if($request->input('nome') == '' || $request->input('nome') == null){
            return redirect()->back()->withErrors('O campo "Nome" não pode estar em branco.');
        }
        
        $viceprefeito = VicePrefeito::findById($id);        
        if($viceprefeito){            
            if($request->file('url_imagem')){                
                if($request->file('url_imagem')->extension() != 'jpg' && $request->file('url_imagem')->extension() != 'jpeg' && $request->file('url_imagem')->extension() != 'png' && $request->file('url_imagem')->extension() != 'gif'){
                    return redirect()->back()->withErrors('O aquivo anexo não é uma imagem.');
                }
                $file = $request->file('url_imagem')->store('viceprefeito', 'public'); //salva doc
            }           

            $atualizar = VicePrefeito::atualizar($request, $id, $file);
            if($atualizar != 0){
                return redirect('/phpmyadmin/restrito/viceprefeito');
            } else {            
                return redirect()->back()->withErrors('Houve um erro ao salvar as informações.');
            }            
        }
    }
}
