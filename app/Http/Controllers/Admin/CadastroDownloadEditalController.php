<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositorio\CadastroDownloadEdital;

class CadastroDownloadEditalController extends Controller
{
    protected function index($id){
        $downloads = CadastroDownloadEdital::findAllById($id);
        return View('Admin.Edital.downloads', compact('downloads'));
    }

    protected function find($id){
        $download = CadastroDownloadEdital::find($id);
        return View('Admin.Edital.download-info', compact('download'));
    }
}
