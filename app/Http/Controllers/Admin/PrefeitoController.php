<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Repositorio\Prefeito;

class PrefeitoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function prefeito(){
        $prefeito = Prefeito::all();
        return View('Admin.Prefeito.prefeito', compact('prefeito'));
    }

    protected function update(Request $request, $id){
        $file ='';
        if($request->input('nome') == '' || $request->input('nome') == null){
            return redirect()->back()->withErrors('O campo "Nome" não pode estar em branco.');
        }
        
        $prefeito = Prefeito::findById($id);        
        if($prefeito){            
            if($request->file('url_imagem')){                
                if($request->file('url_imagem')->extension() != 'jpg' && $request->file('url_imagem')->extension() != 'jpeg' && $request->file('url_imagem')->extension() != 'png' && $request->file('url_imagem')->extension() != 'gif'){
                    return redirect()->back()->withErrors('O aquivo anexo não é uma imagem.');
                }
                $file = $request->file('url_imagem')->store('prefeito', 'public'); //salva doc
            }           

            $atualizar = Prefeito::atualizar($request, $id, $file);
            if($atualizar != 0){
                return redirect('/phpmyadmin/restrito/prefeito');
            } else {            
                return redirect()->back()->withErrors('Houve um erro ao salvar as informações.');
            }            
        }
    }
}
