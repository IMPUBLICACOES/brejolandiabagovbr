<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositorio\Aviso;
use Illuminate\Support\Facades\Storage;
use File;

class AvisoController extends Controller
{
    protected function index(){
        $avisos = Aviso::all();
        return View('Admin.Aviso.index', compact('avisos'));
    }

    protected function create(Request $request){
        $request->validate([
            'texto' => 'required',
            'titulo' => 'required|max:191',
            'url_imagem' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $create = Aviso::criar($request);

        if($create){
            return redirect('/phpmyadmin/restrito/aviso');
        } else {
            return redirect()->back()->withErrors('Houve um erro ao adicionar um aviso.');
        }
    }

    protected function delete($id){
        $aviso = Aviso::find($id);
        if($aviso){
            if($aviso->url_imagem){
                if(Storage::disk('public')->exists($aviso->url_imagem)){
                    Storage::disk('public')->delete($aviso->url_imagem);
                }
            }
            $aviso->delete();
        }
        return redirect('/phpmyadmin/restrito/aviso');
    }
}
