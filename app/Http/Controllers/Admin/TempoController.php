<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Repositorio\Previsao;
use App\Http\Controllers\Controller;

class TempoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function tempo(){
        $previsao = Previsao::all();
        return View('Admin.tempo', compact('previsao'));
    }

    protected function update(Request $request, $id){
        $previsao = Previsao::find($id);
        if($previsao){
            if($request){
                $previsao->codigo_municipio = $request->input('codigo_municipio');
                $previsao->chave_sistema = $request->input('chave_sistema');                
                $salvo = $previsao->save();

                if($salvo){
                    return redirect('phpmyadmin/restrito/tempo');
                } 
                return redirect()->back()->withErrors('Houve um erro ao atualizar as informações.');
            }
        }
        return redirect('phpmyadmin/restrito/tempo');
    }
}
