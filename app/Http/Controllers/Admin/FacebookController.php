<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Repositorio\Facebook;
use App\Http\Controllers\Controller;

class FacebookController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function facebook(){
        $face = Facebook::all();
        return View('Admin.facebook', compact('face'));
    }

    protected function update(Request $request, $id){
        $face = Facebook::find($id);
        if($face){
            if($request){
                $face->url = $request->input('url');
                $salvo = $face->save();

                if($salvo){
                    return redirect('phpmyadmin/restrito/facebook');
                } 
                return redirect()->back()->withErrors('Houve um erro ao atualizar as informações.');
            }
        }
        return redirect('phpmyadmin/restrito/facebook');
    }
}
