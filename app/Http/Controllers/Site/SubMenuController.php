<?php

namespace App\Http\Controllers\Site;

use App\Repositorio\SubMenu;
use App\Http\Controllers\Controller;

class SubMenuController extends Controller
{
    protected function submenus(){
        return SubMenu::all();
    }
}
