<?php

namespace App\Http\Controllers\Site;

use App\Repositorio\CorPadrao;
use App\Http\Controllers\Controller;

class CorPadraoController extends Controller
{
    protected function corpadrao(){
        foreach (CorPadrao::all() as $cor){
            return $cor->cor_hexa;
        }
    }
}
