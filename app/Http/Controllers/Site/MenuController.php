<?php

namespace App\Http\Controllers\Site;

use App\Repositorio\Menu;
use App\Http\Controllers\Controller;

class MenuController extends Controller
{
    protected function menus(){
        return Menu::all();
    }
}
