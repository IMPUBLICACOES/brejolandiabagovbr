<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Repositorio\Previsao;

class PrevisaoController extends Controller
{
    protected function all(){
        $prev = Previsao::contents();
        return $prev["results"];
    }
}
