<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Repositorio\Aviso;
use App\Http\Controllers\Controller;

class AvisoController extends Controller
{
    public function index(){
        return Aviso::orderBy('id', 'DESC')->get();
    }
}
