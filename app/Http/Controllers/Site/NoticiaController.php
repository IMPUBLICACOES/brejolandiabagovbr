<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Repositorio\Noticia;

class NoticiaController extends Controller
{
    protected function noticias6(){
        return Noticia::orderBy('created_at', 'DESC')->limit(6)->get();
    }

    protected function noticias4(){
        return Noticia::orderBy('created_at', 'DESC')->limit(4)->get();
    }

    protected function noticias5(){
        return Noticia::orderBy('created_at', 'DESC')->limit(5)->get();
    }

    protected function find($id){
        return Noticia::find($id);
    }

    protected function all(){
        return Noticia::orderBy('created_at', 'DESC')->paginate(5);
    }
}
