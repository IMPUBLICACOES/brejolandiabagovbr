<?php

namespace App\Http\Controllers\Site;

use App\Repositorio\Licitacao;
use App\Http\Controllers\Controller;

class LicitacaoEditalController extends Controller
{
    protected function all(){        
        return Licitacao::orderBy('data', 'DESC')->get();
    }
    
    protected function paginate(){
        return Licitacao::orderBy('data', 'DESC')->paginate(5);
    }
    
}
 