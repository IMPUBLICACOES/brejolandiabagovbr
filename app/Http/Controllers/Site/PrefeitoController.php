<?php

namespace App\Http\Controllers\Site;

use App\Repositorio\Prefeito;
use App\Http\Controllers\Controller;

class PrefeitoController extends Controller
{
    protected function prefeito(){
        return Prefeito::all();
    }
}
