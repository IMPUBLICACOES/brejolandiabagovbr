<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Repositorio\EstruturaAdministrativa;
use App\Repositorio\UnidadeAdministrativa;

class EstruturaAdministrativaController extends Controller
{
    protected function estruturaAdministrativa(){
        return EstruturaAdministrativa::all();
    }

    protected function unidadeAdministrativa(){
        return UnidadeAdministrativa::all();
    }

    protected function estruturaAdministrativaFind($id){
        return EstruturaAdministrativa::find($id);
    }

    protected function unidadeAdministrativaFind($id){
        return UnidadeAdministrativa::find($id);
    }
}