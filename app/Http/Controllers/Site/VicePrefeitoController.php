<?php

namespace App\Http\Controllers\Site;

use App\Repositorio\VicePrefeito;
use App\Http\Controllers\Controller;

class VicePrefeitoController extends Controller
{
    protected function vice(){
        return VicePrefeito::all();        
    }
}
