<?php

namespace App\Http\Controllers\Site;

use App\Repositorio\Municipio;
use App\Http\Controllers\Controller;

class MunicipioController extends Controller
{
    protected function municipio(){
        return Municipio::all();
    }
}
