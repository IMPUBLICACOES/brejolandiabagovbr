<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class PermissaoAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            if(Auth::user()->permissao){
                return $next($request);
            } else {
                Auth::logout();
                return redirect('phpmyadmin/restrito/login');    
            }
        } else {
            return redirect('phpmyadmin/restrito/login');
        }
    }
}
