@extends('layouts.app')

@section('content')
<div class="container">    
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin my-5">
            <div class="card-body">
                <h5 class="card-title text-center">Registrar</h5>
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="form-label-group">
                        <input name="name" type="name" id="name" class="form-control @error('name') is-invalid @enderror" placeholder="Nome" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label for="name">Nome</label>
                    </div>
                    <div class="form-label-group">
                        <input name="email" type="email" id="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label for="email">E-mail</label>
                    </div>
                    <div class="form-label-group">              
                        <input type="password" id="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Senha" required autocomplete="current-password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label for="password">Senha</label>
                    </div>
                    <div class="form-label-group">              
                        <input class="form-control @error('password') is-invalid @enderror" placeholder="Senha" id="password-confirm" type="password" name="password_confirmation" required autocomplete="new-password">
                        @error('password-confirm')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label for="password-confirm">Confirme a senha</label>
                    </div>                                       
                    <button class="btn btn-lg btn-secondary btn-block text-uppercase" type="submit">Registrar</button><br/>
                    <div class="card card-signin ">    
                        <div class="d-flex justify-content-center">
                            @if (Route::has('password.request'))
                                <a class="btn btn-link text-center text-body" href="{{ route('password.request') }}">
                                    {{ __('Esqueceu sua senha?') }}
                                </a>
                            @endif
                        </div>
                    </div>
                </form>
            </div> <!-- card-body -->
        </div> <!-- card card-signin my-5 -->
      </div> <!-- col-sm-9 col-md-7 col-lg-5 mx-autos -->
    </div> <!-- row -->
</div>
@endsection
