<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('css/app.css')}}">

        <!-- Icons -->
        <link href="vendor/site/icons/fontawesome-free-5.9.0-web/css/all.css" rel="stylesheet">

        <title>Prefeitura de Brejolândia</title>
        <link rel="icon" type="image/png" href="imagens/logos/brasao-municipio.png" />
    </head>
    <body>
        <div id="app">
            <navbar-7-component></navbar-7-component>
            @yield('app')
            <footer-1-component></footer-1-component>
            <aviso-1-component></aviso-1-component>
        </div>
    </body>
    <script src="{{asset('js/app.js')}}"></script>
</html>
