@extends('Admin.Main.app')
@section('app')  
    <div class="container-fluid">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex justify-content-between">
                    <h4 class="page-title">EDITAR NOTÍCIA</h4>
                </div>
            </div><!-- row -->
        </div><!-- page-breadcrumb --><br/>
        @if($errors->any()) 
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12">
                    <div class="alert alert-danger" role="alert">
                        <span>{{$errors->first()}}</span>
                    </div>
                </div>
            </div><!-- row -->
        @endif    
    
        <div class="row card">
            <div class="col-md-12">
                <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/noticias/update', $noticia->id)}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="form-group">
                        <label>TÍTULO:</label>
                        <input type="text" name="nome" class="form-control" value="{{$noticia->nome}}">
                    </div>
                    <div class="form-group">
                        <label>DESCRIÇÃO:</label>
                        <input type="text" name="descricao" class="form-control" value="{{$noticia->descricao}}">
                    </div>
                    <div class="form-group">                       
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">TIPO</label>
                            </div>
                            <select class="custom-select custom-select" name="tipo">
                                @foreach ($tipo as $t)
                                    @if ($noticia->tipo == $t->id)
                                        <option value="{{$t->id}}" selected>{{$t->nome}}</option>
                                    @else
                                            <option value="{{$t->id}}">{{$t->nome}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>  
                    </div>
                    <div class="form-group">
                        <label>ORGÃO EMISSOR:</label>
                        <input type="text" name="orgao_emissor" class="form-control" value="{{$noticia->orgao_emissor}}">
                    </div>
                    
                    <div class="form-group">
                        <label>IMAGEM:</label>
                        <input type="file" name="url_imagem[]" class="form-control" multiple>
                    </div>
                    <div class="form-group">
                        <label>TEXTO:</label>
                        <textarea class="form-control" name="texto" rows="6">{{$noticia->texto}}</textarea>
                    </div>
                    <div class="box-footer d-flex justify-content-between">
                        <button type="submit" class="btn btn-success pull-right">
                            <i class="fas fa-wrench"></i>
                            <span>ATUALIZAR</span>
                        </button>
                        <a href="/phpmyadmin/restrito/noticias"class="btn btn-secondary" data-dismiss="modal">
                            <i class="far fa-window-close"></i>
                            <sapn>CANCELAR</sapn>
                        </a>
                    </div>
                </form>
            </div><!-- col-md-12 -->
        </div><!-- row -->
        
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="d-flex justify-content-end">
                    <a href="{{URL::to('/phpmyadmin/restrito/noticias/imagem/deleteall', $noticia->id)}}" class="btn btn-danger pull-right">
                        <i class="far fa-trash-alt"></i>
                        <span>APAGAR FOTOS</sapn>
                    </a>
                </div><!-- d-flex justify-content-end -->
            </div><!-- col-xs-12 col-md-12 col-lg-12 -->
        </div><!-- row -->
    
        <div class="row">
            @foreach($noticia->url_imagens as $img)
                <div class="col-md-3 col-lg-3 col-sm-12">
                    <div class="d-flex justify-content-end">
                        <a type="button" href="{{URL::to('/phpmyadmin/restrito/noticias/imagem/delete', $img->id)}}" class="close" aria-label="Fechar">
                            <span aria-hidden="true">&times;</span>
                        </a>                                
                    </div> 
                    <img src="{{url('storage/' . $img->url_imagem)}}" class="rounded" width="200" height="200">
                </div><!-- col-md-3 col-lg-3 col-sm-12 -->
            @endforeach
        </div><!-- row -->
    </div><!-- container-fluid -->
@stop