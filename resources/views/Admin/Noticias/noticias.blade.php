@extends('Admin.Main.app')
@section('app')  
    <div class="container-fluid">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex justify-content-between">
                    <h4 class="page-title">NOTÍCIAS</h4>                    
                    <a type="submit" class="btn btn-success pull-right" data-toggle="modal" data-target="#modalNoticias">
                        <i class="fas fa-plus-square"></i>
                        <span>NOVO</span>
                    </a>                    
                </div>
            </div><!-- row -->
        </div><!-- page-breadcrumb --><br/>      
        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="modalNoticias" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content card">                    
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>                                
                        </div>                        
                        <div class="card-body">                            
                            <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/noticias/create/')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="form-group">
                                    <label>TÍTULO:</label>
                                    <input type="text" name="nome" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>DESCRIÇÃO:</label>
                                    <input type="text" name="descricao" class="form-control">
                                </div>
                                <div class="form-group">                                    
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text" for="inputGroupSelect01">TIPO</label>
                                        </div>
                                        <select class="custom-select custom-select" name="tipo">
                                            @foreach ($tipo as $t)
                                                <option value="{{$t->id}}">{{$t->nome}}</option>                                                                    
                                             @endforeach                                                                   
                                        </select>
                                    </div>
                                </div>                        
                                <div class="form-group">
                                    <label>ORGÃO EMISSOR:</label>
                                    <input type="text" name="orgao_emissor" class="form-control">
                                </div>
                                
                                <div class="form-group">
                                    <label>IMAGEM:</label>
                                    <input type="file" name="url_imagem[]" class="form-control" multiple>
                                </div>
                                <div class="form-group">
                                    <label>TEXTO:</label>
                                    <textarea class="form-control" name="texto" rows="6"></textarea>
                                </div>
                                <div class="box-footer d-flex justify-content-between">
                                    <button type="submit" class="btn btn-success pull-right">
                                        <i class="fas fa-plus-square"></i>
                                        <span>CRIAR</span>
                                    </button>
                                    <div></div>                                  
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                        <i class="far fa-window-close"></i>
                                        <span>CANCELAR</span>
                                    </button>
                                </div>                              
                            </form>                            
                        </div>
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </div><!-- modal fade -->
        </div><!-- row -->

        <div class="row">                
            <div class="col-xs-12 col-md-12 col-lg-12">                
                @if($errors->any())                        
                <div class="alert alert-danger" role="alert">                                                    
                    <span>{{$errors->first()}}</span>
                </div>                        
                @endif                    
            </div><!-- col-xs-12 col-md-12 col-lg-12 -->
            <table class="table table-striped table-hover">
                <thead>
                     <tr>                    
                        <th scope="col">TÍTULO</th>                                                        
                        <th scope="col">DATA</th>
                        <th scope="col"></th>
                        <th scope="col">ORGÃO EMISSOR</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($noticias as $n)
                    <tr class="box">
                        <th scope="col">{{$n->nome}}</th>                            
                        <th scope="col">{{$n->data}}</th>
                        <th scope="col"></th>
                        <th scope="col">{{$n->orgao_emissors}}</th>
                        <th scope="col"></th>
                        <th scope="col">                            
                            <form action="{{URL::to('/phpmyadmin/restrito/noticias/excluir', $n->id )}}" method="GET">
                                <button type="submit" class="btn btn-danger pull-right border border-secondary text-dark" >
                                    <i class="fas fa-trash-alt"></i>
                                    <span> EXCLUIR </span>                        
                                </button>                        
                            </form>
                        </th>
                        <th scope="col">
                            <a type="submit" href="{{URL::to('/phpmyadmin/restrito/noticias/edit', $n->id )}}" class="btn btn-warning pull-right text-dark">
                                <i class="fas fa-edit"></i>
                                <span> EDITAR </span>
                            </a>
                        </th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="d-flex justify-content-center">
                    <div class="text-center">
                        {{$noticias}}
                    </div><!-- text-center -->
                </div><!-- d-flex justify-content-center -->
            </div><!-- col-xs-12 col-md-12 col-lg-12 -->
    </div><!-- container-fluid -->
@stop