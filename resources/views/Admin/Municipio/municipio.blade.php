@extends('Admin.Main.app')
@section('app')
    <div class="container-fluid">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex no-block align-items-center">
                    <h4 class="page-title">MUNICÍPIO</h4>                            
                </div>
            </div><!-- row -->
        </div><!-- page-breadcrumb --><br/>
        <div class="row">
            <!-- Modal -->
            @foreach ($municipios as $m)
            <div class="modal fade" id="modalMunicipioUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content card">
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>                                
                        </div>
                        <div class="modal-header">
                            <img src="{{URL::to($m->url_imagem)}}" class="card-img-top" width="570" height="150">                            
                        </div><!-- modal-header -->
                        <div class="card-body">                            
                            <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/municipio', $m->id)}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="form-group">
                                    <label>NOME:</label>
                                    <input type="text" name="nome" class="form-control" value="{{$m->nome}}">
                                </div>
                                <div class="form-group">
                                    <label>DESCRIÇÃO:</label>
                                    <input type="text" name="descricao" class="form-control" value="{{$m->descricao}}">
                                </div>                                
                                <div class="form-group">
                                    <label>IMAGEM:</label>
                                    <input type="file" name="url_imagem" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>TEXTO:</label>
                                    <textarea class="form-control" name="texto" rows="12">{{$m->texto}}</textarea>
                                </div>
                                <div class="box-footer d-flex justify-content-between">
                                    <button type="submit" class="btn btn-success pull-right">
                                        <i class="fas fa-wrench"></i>
                                        <span>ATUALIZAR</span>
                                    </button>
                                    <div></div>                                  
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                        <i class="far fa-window-close"></i>
                                        <span>CANCELAR</span>
                                    </button>
                                </div>                                
                            </form>                            
                        </div>                    
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </div><!-- modal fade -->
            @endforeach
            <div class="col-md-12">
                @foreach ($municipios as $m)
                <div class="card flex-md-row mb-4 shadow-sm h-md-250">
                    <div class="card-body d-flex flex-column align-items-start">
                        <strong class="d-inline-block mb-2 text-success">{{$m->updated_at}}</strong>
                        <h3 class="mb-0">
                            <a class="text-dark" href="#">{{$m->nome}}</a>
                        </h3><br>
                        <p class="card-text mb-auto">{{$m->texto_resumido}}</p>
                        <div class="col-xs-12">                
                            <a type="submit" class="btn btn-warning" data-toggle="modal" data-target="#modalMunicipioUpdate">
                                <i class="fas fa-edit"></i>
                                <span>EDITAR</span>
                            </a>
                        </div> 
                    </div>
                    <img class="card-img-right flex-auto d-none d-lg-block" data-src="holder.js/200x250?theme=thumb" alt="Thumbnail [200x250]" src="{{URL::to($m->url_imagem)}}" data-holder-rendered="true" style="width: 200px; height: 250px;">
                </div>   
                @endforeach                                            
            </div><!-- col-md-12 -->
        </div><!-- row -->    
    </div>  <!-- container-fluid -->    
@endsection