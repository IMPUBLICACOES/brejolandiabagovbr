@extends('Admin.Main.app')
@section('app')  
    <div class="container-fluid">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex justify-content-between">
                    <h4 class="page-title">EDITAR UNIDADE ADMINISTRATIVA</h4>                                                    
                </div>
            </div><!-- row -->
        </div><!-- page-breadcrumb --><br/>              

        <div class="row card">
            <div class="col-md-12">                
                @if($errors->any())                                 
                    <div class="col-xs-12 col-md-12 col-lg-12">                
                        <div class="alert alert-danger" role="alert">                                                    
                            <span>{{$errors->first()}}</span>
                        </div>                        
                    </div>            
                @endif
                @foreach($unidadeadministrativa as $ua)      
                   <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/unidadeadministrativa/update', $ua->id)}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="form-group">
                            <label>NOME:</label>
                            <input type="text" name="nome" class="form-control" value="{{$ua->nome}}">
                        </div>
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="inputGroupSelect01">ENTIDADE</label>
                                </div>
                                <select class="custom-select custom-select" name="id_estrutura_administrativa">
                                    @foreach ($ea as $e)
                                        @if($e->id == $ua->id_estrutura_administrativa)
                                            <option value="{{$e->id}}" selected>{{$e->nome}}</option>
                                        @else
                                            @if($e->unidades_administrativas)
                                                <option value="{{$e->id}}">{{$e->nome}}</option>                                        
                                            @endif
                                        @endif
                                    @endforeach
                                </select>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <label>RESPONSÁVEL:</label>
                            <input type="text" name="responsavel" class="form-control" value="{{$ua->responsavel}}">
                        </div>
                        <div class="form-group">
                            <label>ENDEREÇO:</label>
                            <input type="text" name="endereco" class="form-control" value="{{$ua->endereco}}">
                        </div>
                        <div class="form-group">
                            <label>CEP:</label>
                            <input type="text" name="cep" class="form-control" value="{{$ua->cep}}">
                        </div>
                        <div class="form-group">
                            <label>FUNCIONAMENTO:</label>
                            <input type="text" name="hr_funcionamento" class="form-control" value="{{$ua->hr_funcionamento}}">
                        </div>
                        <div class="form-group">
                            <label>TELEFONE:</label>
                            <input type="text" name="telefone" class="form-control" value="{{$ua->telefone}}">
                        </div>
                        <div class="form-group">
                            <label>E-MAIL:</label>
                            <input type="text" name="email" class="form-control" value="{{$ua->email}}">
                        </div>
                        <div class="form-group">
                            <label>IMAGEM:</label>
                            <input type="file" name="url_imagem" class="form-control" value="{{$ua->url_imagem}}">
                        </div>
                        <div class="form-group">
                            <label>COMPETÊNCIAS:</label>
                            <textarea class="form-control" name="competencias" rows="6">{{$ua->competencias}}</textarea>
                        </div>                        
                        <div class="box-footer d-flex justify-content-between">
                            <button type="submit" class="btn btn-success pull-right">
                                <i class="fas fa-wrench"></i>
                                <span>ATUALIZAR</span>
                            </button>
                            <a href="/phpmyadmin/restrito/unidadeadministrativa" class="btn btn-secondary" data-dismiss="modal">
                                <i class="far fa-window-close"></i>
                                <sapn>CANCELAR</sapn>
                            </a>
                        </div>                            
                    </form> 
                @endforeach
            </div><!-- col-md-12 -->        
        </div><!-- row -->
    </div><!-- container-fluid -->
@stop