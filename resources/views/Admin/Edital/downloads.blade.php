@extends('Admin.Main.app')
@section('app')  
    <div class="container-fluid">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex justify-content-between">
                    <h4 class="page-title">DOWNLOADS</h4>
                </div>
            </div><!-- row -->
        </div><!-- page-breadcrumb --><br/>
        <div class="row">                
            <div class="col-xs-12 col-md-12 col-lg-12">                
                @if($errors->any())                        
                <div class="alert alert-danger" role="alert">                                                    
                    <span>{{$errors->first()}}</span>
                </div>                        
                @endif                    
            </div><!-- col-xs-12 col-md-12 col-lg-12 -->
            <table class="table">
                <thead class="bg-info">
                    <tr>                    
                        <th scope="col">DATA DO DOWNLOAD</th>
                        <th scope="col">FORNECEDOR</th>
                        <th scope="col">REPRESENTANTE</th>
                        <th scope="col">TELEFONE</th>
                        <th scope="col"></th>
                    </tr>
                </thead><br>
                <tbody>
                    @foreach($downloads as $d)
                        <tr>                    
                            <th scope="col">{{$d->created_at}}</th>
                            <th scope="col">{{$d->fornecedor}}</th>
                            <th scope="col">{{$d->nome_representante}}</th>                                                        
                            <th scope="col">{{$d->telefone}}</th>                                
                            <th scope="col">
                                <a type="submit" href="{{URL::to('/phpmyadmin/restrito/downloads/find', $d->id )}}" class="btn btn-info pull-right" >
                                    <span> INFO </span>
                                </a>
                            </th>
                        </tr>
                    @endforeach
                </tbody>
            </table>  
        </div><!-- row -->
    </div><!-- container-fluid -->
@stop