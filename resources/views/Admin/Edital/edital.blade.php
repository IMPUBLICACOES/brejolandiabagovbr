@extends('Admin.Main.app')
@section('app')  
    <div class="container-fluid">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex justify-content-between">
                    <h4 class="page-title">EDITAIS</h4>                    
                    <a type="submit" class="btn btn-success pull-right" data-toggle="modal" data-target="#modalEdital">
                        <i class="fas fa-plus-square"></i>
                        <span>NOVO</span>
                    </a>                    
                </div>
            </div><!-- row -->
        </div><!-- page-breadcrumb --><br/>      
        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="modalEdital" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content card">                    
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>                                
                        </div>                        
                        <div class="card-body">                            
                            <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/edital/create/')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="form-group">
                                    <label>MODALIDADE:</label>
                                    <input type="text" name="modalidade" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>DATA DA LICITAÇÃO:</label>                                    
                                    <div class='input-group date'>
                                        <input name="data_edital" type='date' id="date" class="form-control"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>                                
                                <div class="form-group">
                                    <label>EDITAL:</label>
                                    <input type="file" name="url_edital[]" class="form-control" multiple>
                                </div>
                                <div class="form-group">
                                    <label>OBJETO:</label>
                                    <textarea class="form-control" name="objeto" rows="6"></textarea>
                                </div>
                                <div class="box-footer d-flex justify-content-between">
                                    <button type="submit" class="btn btn-success pull-right">
                                        <i class="fas fa-plus-square"></i>
                                        <span>CRIAR</span>
                                    </button>
                                    <div></div>                                  
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                        <i class="far fa-window-close"></i>
                                        <span>CANCELAR</span>
                                    </button>
                                </div>                              
                            </form>                            
                        </div>
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </div><!-- modal fade -->
        </div><!-- row -->

        <div class="row">                
            <div class="col-xs-12 col-md-12 col-lg-12">                
                @if($errors->any())                        
                <div class="alert alert-danger" role="alert">                                                    
                    <span>{{$errors->first()}}</span>
                </div>                        
                @endif                    
            </div><!-- col-xs-12 col-md-12 col-lg-12 -->
            <table class="table table-striped table-hover">
                <thead>
                    <tr>                    
                        <th scope="col">DATA</th>
                        <th scope="col">MODALIDADE</th>                        
                        <th scope="col">OBJETO</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>                                
                    @foreach($edital as $n)
                        <tr class="box">
                            <th scope="col">{{$n->data_convertida}}</th>                            
                            <th scope="col">{{$n->modalidade}}</th>                            
                            <th scope="col">{{$n->objeto}}</th>
                            <th scope="col">                                
                                <a type="submit" href="{{URL::to('/phpmyadmin/restrito/downloads', $n->id )}}" class="btn btn-info pull-right text-dark">
                                    <i class="fas fa-download"></i>
                                    <span> DOWNLOADS </span>
                                </a>
                            </th>
                            <th scope="col">                            
                                <form action="{{URL::to('/phpmyadmin/restrito/edital/excluir', $n->id )}}" method="GET">
                                    <button type="submit" class="btn btn-danger pull-right border border-secondary text-dark" >
                                        <i class="fas fa-trash-alt"></i>
                                        <span> EXCLUIR </span>
                                    </button>
                                </form>
                            </th>
                            <th scope="col">                                
                                <a type="submit" href="{{URL::to('/phpmyadmin/restrito/edital/edit', $n->id )}}" class="btn btn-warning pull-right text-dark">
                                    <i class="fas fa-edit"></i>
                                    <span> EDITAR </span>
                                </a>
                            </th>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="d-flex justify-content-center">
                    <div class="text-center">
                        {{$edital}}
                    </div><!-- text-center -->
                </div><!-- d-flex justify-content-center -->
            </div><!-- col-xs-12 col-md-12 col-lg-12 -->
    </div><!-- container-fluid -->
@stop