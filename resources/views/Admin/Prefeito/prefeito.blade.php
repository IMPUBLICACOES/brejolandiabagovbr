@extends('Admin.Main.app')
@section('app')  
    <div class="container-fluid">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex no-block align-items-center">
                    <h4 class="page-title">PREFEITO</h4>
                </div>
            </div><!-- row -->
        </div><!-- page-breadcrumb --><br/>
        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="modalPrefeito" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content card">
                    @foreach ($prefeito as $p)
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>                                
                        </div>
                        <div class="modal-header">
                            <img src="{{URL::to($p->url_imagem)}}" class="card-img-top" width="570" height="150">                            
                        </div><!-- modal-header -->
                        <div class="card-body">                            
                            <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/prefeito', $p->id)}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="form-group">
                                    <label>Nome:</label>
                                    <input type="text" name="nome" class="form-control" value="{{$p->nome}}">
                                </div>
                                <div class="form-group">
                                    <label>Descrição:</label>
                                    <input type="text" name="descricao" class="form-control" value="{{$p->descricao}}">
                                </div>
                                <div class="form-group">
                                    <label>Imagem:</label>
                                    <input type="file" name="url_imagem" class="form-control" value="{{$p->url_imagem}}">
                                </div>
                                <div class="form-group">
                                    <label>Texto:</label>
                                    <textarea class="form-control" name="texto" rows="6">{{$p->texto}}</textarea>
                                </div>
                                <div class="box-footer d-flex justify-content-between">
                                    <button type="submit" class="btn btn-success pull-right">
                                        <i class="fas fa-wrench"></i>
                                        <span>ATUALIZAR</span>
                                    </button>
                                    <div></div>                                  
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                        <i class="far fa-window-close"></i>
                                        <span>CANCELAR</span>
                                    </button>
                                </div>                                  
                            </form>                            
                        </div>
                    @endforeach
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </div><!-- modal fade -->            
            <div class="col-md-12">
            @foreach ($prefeito as $p)
                <div class="col-xs-12 col-md-12 col-lg-12">                
                    @if($errors->any())                        
                    <div class="alert alert-danger" role="alert">                                                    
                        <span>{{$errors->first()}}</span>
                    </div>                        
                    @endif                    
                </div>
                <div class="card flex-md-row mb-4 shadow-sm h-md-250">
                    <div class="card-body d-flex flex-column align-items-start">
                        <strong class="d-inline-block mb-2 text-success">{{$p->updated_at}}</strong>
                        <h3 class="mb-0">
                            <a class="text-dark" href="#">{{$p->nome}}</a>
                        </h3><br>
                        <p class="card-text mb-auto">{{$p->texto_resumido}}</p>
                        <div class="col-xs-12">                
                            <a type="submit" class="btn btn-warning" data-toggle="modal" data-target="#modalPrefeito">
                                <i class="fas fa-edit"></i>
                                <span>EDITAR</span>
                            </a>
                        </div> 
                    </div>
                    <img class="card-img-right flex-auto d-none d-lg-block" data-src="holder.js/200x250?theme=thumb" alt="Thumbnail [200x250]" src="{{URL::to($p->url_imagem)}}" data-holder-rendered="true" style="width: 200px; height: 250px;">
                </div>   
                @endforeach                                            
            </div><!-- col-md-12 -->
        </div><!-- row -->     
    </div><!-- container-fluid -->  
@stop