@extends('Admin.Main.app')
@section('app')  
    <div class="container-fluid">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex justify-content-between">
                    <h4 class="page-title">NOTÍCIAS</h4>                    
                    <a type="submit" class="btn btn-success pull-right" data-toggle="modal" data-target="#modalContato">
                        <i class="fas fa-plus-square"></i>
                        <span>NOVO</span>
                    </a>                    
                </div>
            </div><!-- row -->
        </div><!-- page-breadcrumb --><br/> 

        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="modalContato" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content card">                    
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>                                
                        </div>                        
                        <div class="card-body">                                                        
                            <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/contato/create')}}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="form-group">
                                    <label>NOME:</label>
                                    <input type="text" name="nome" class="form-control">
                                </div>                                
                                <div class="form-group">
                                    <label>NÚMERO:</label>
                                    <input type="text" name="numero" class="form-control">
                                </div>                                
                                <div class="box-footer d-flex justify-content-between">
                                    <button type="submit" class="btn btn-success pull-right">
                                        <i class="fas fa-plus-square text-body"></i>
                                        <span>CRIAR</span>
                                    </button>
                                    <div></div>                                  
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                        <i class="far fa-window-close"></i>
                                        <span>CANCELAR</span>
                                    </button>
                                </div>                              
                            </form>                            
                        </div>                    
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </div><!-- modal fade -->
        </div><!-- row -->       
    
        <div class="row">                
            @if($errors->any())     
                <div class="col-xs-12 col-md-12 col-lg-12">                
                    <div class="alert alert-danger" role="alert">                                                    
                        <span>{{$errors->first()}}</span>
                    </div>                        
                </div>            
            @endif
            <div class="col-xs-12 col-md-12 col-lg-12">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>                    
                            <th scope="col">NOME</th>                                                        
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col">NÚMERO</th>                                
                            <th scope="col"></th>
                            <th scope="col"></th>                            
                            
                        </tr>
                    </thead><br>
                    <tbody>
                        @foreach($contatos as $c)
                        <tr class="box">
                            <th scope="col">{{$c->nome}}</th>                                                        
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col">{{$c->numero}}</th>
                            <th scope="col">                            
                                <form action="{{URL::to('/phpmyadmin/restrito/contato/excluir', $c->id )}}" method="GET">
                                    <button type="submit" class="btn btn-danger pull-right border border-secondary text-dark" >
                                        <i class="fas fa-trash-alt"></i>
                                        <span> EXCLUIR </span>                        
                                    </button>                        
                                </form>
                            </th>
                            <th scope="col">
                                <a type="submit" href="{{URL::to('/phpmyadmin/restrito/contato/edit', $c->id )}}" class="btn btn-warning pull-right text-dark" >
                                    <i class="fas fa-edit"></i>
                                    <span> EDITAR </span>
                                </a>
                            </th>
                        </tr>
                        @endforeach                            
                    </tbody>
                </table>
            </div><!-- col-xs-12 col-md-12 col-lg-12 -->        
        </div><!-- row -->        
    </div><!-- container-fluid -->        
@stop