@extends('Admin.Main.app')
@section('app')  
    <div class="container-fluid">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex justify-content-between">
                    <h4 class="page-title">EDITAR CONTATO</h4>
                </div>
            </div><!-- row -->
        </div><!-- page-breadcrumb --><br/>
        @if($errors->any()) 
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12">
                    <div class="alert alert-danger" role="alert">
                        <span>{{$errors->first()}}</span>
                    </div>
                </div>
            </div><!-- row -->
        @endif 
        <div class="row card">
            <div class="col-md-12">        
                <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/contato/update', $contato->id)}}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="form-group">
                        <label>NOME:</label>
                        <input type="text" name="nome" class="form-control" value="{{$contato->nome}}">
                    </div>                        
                    <div class="form-group">
                        <label>NÚMERO:</label>
                        <input type="text" name="numero" class="form-control" value="{{$contato->numero}}">
                    </div>                       
                    <div class="box-footer d-flex justify-content-between">
                        <button type="submit" class="btn btn-success pull-right">
                            <i class="fas fa-wrench"></i>
                            <span>ATUALIZAR</span>
                        </button>
                        <a href="/phpmyadmin/restrito/contato"class="btn btn-secondary" data-dismiss="modal">
                            <i class="far fa-window-close"></i>
                            <sapn>CANCELAR</sapn>
                        </a>
                    </div>                           
                </form> 
            </div><!-- col-md-12 -->        
        </div><!-- row -->
    </div><!-- container-fluid -->  
@stop