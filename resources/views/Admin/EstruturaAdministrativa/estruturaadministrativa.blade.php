@extends('Admin.Main.app')
@section('app')  
    <div class="container-fluid">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex justify-content-between">
                    <h4 class="page-title">ESTRUTURA ADMINISTRATIVA</h4>                    
                    <a type="submit" class="btn btn-success pull-right" data-toggle="modal" data-target="#modalEstruturaAdministrativa">
                        <i class="fas fa-plus-square"></i>
                        <span>NOVO</span>
                    </a>                    
                </div>
            </div><!-- row -->
        </div><!-- page-breadcrumb --><br/>      
        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="modalEstruturaAdministrativa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content card">                    
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>                                
                        </div>                        
                        <div class="card-body">                            
                            <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/estruturaadministrativa/create')}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="form-group">
                                    <label>NOME:</label>
                                    <input type="text" name="nome" class="form-control">
                                </div>
                                <div class="form-group">                                    
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text" for="inputGroupSelect01">UNIDADES ADMINISTRATIVAS</label>
                                        </div>
                                        <select class="custom-select custom-select" name="unidades_administrativas">                                        
                                            <option value="0" selected>NÃO</option>
                                            <option value="1">SIM</option>                                        
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>RESPONSÁVEL:</label>
                                    <input type="text" name="responsavel" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>ENDEREÇO:</label>
                                    <input type="text" name="endereco" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>CEP:</label>
                                    <input type="text" name="cep" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>FUNCIONAMENTO:</label>
                                    <input type="text" name="hr_funcionamento" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>TELEFONE:</label>
                                    <input type="text" name="telefone" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>E-MAIL:</label>
                                    <input type="text" name="email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>IMAGEM:</label>
                                    <input type="file" name="url_imagem" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>COMPETÊNCIAS:</label>
                                    <textarea class="form-control" name="competencias" rows="6"></textarea>
                                </div>
                                <div class="box-footer d-flex justify-content-between">
                                    <button type="submit" class="btn btn-success pull-right">
                                        <i class="fas fa-plus-square"></i>
                                        <span>CRIAR</span>
                                    </button>
                                    <div></div>                                  
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                        <i class="far fa-window-close"></i>
                                        <span>CANCELAR</span>
                                    </button>
                                </div>                                
                            </form>                            
                        </div>                    
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </div><!-- modal fade -->
        </div><!-- row -->

        <div class="row">
            @if($errors->any())
                <div class="col-xs-12 col-md-12 col-lg-12">
                    <div class="alert alert-danger" role="alert">
                        <span>{{$errors->first()}}</span>
                    </div>
                </div>
            @endif
                
            <div class="col-md-12">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th scope="col">NOME</th>
                            <th scope="col">RESPONSÁVEL</th>
                            <th scope="col" class="text-center">SUB-ENTIDADES</th>                                                                                              
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($estruturaadministrativa as $ea)
                        <tr>
                            <th scope="row">{{$ea->nome}}</th>
                            <td scope="col">{{$ea->responsavel}}</td>
                            <td scope="col" class="text-center">
                                <span class="badge badge-pill badge-info">
                                    {{$ea->qt_unidade}}
                                </span>
                            </td>                                                                                                                                                       
                            <td scope="col">
                                <form action="{{URL::to('/phpmyadmin/restrito/estruturaadministrativa/excluir', $ea->id )}}" method="GET" enctype="multipart/form-data">
                                    <button type="submit" class="btn btn-danger pull-right border border-secondary text-dark">
                                        <i class="fas fa-trash-alt"></i>
                                        <span> EXCLUIR </span>                        
                                    </button>                        
                                </form>        
                            </td>
                            <td scope="col">
                                <a type="submit" href="{{URL::to('/phpmyadmin/restrito/estruturaadministrativa/edit', $ea->id )}}" class="btn btn-warning pull-right text-dark" >
                                    <i class="fas fa-edit"></i>
                                    <span> EDITAR </span>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- col-md-12 -->               
        </div><!-- row -->                    
    </div><!-- container-fluid -->
@stop