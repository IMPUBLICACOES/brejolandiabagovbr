@extends('Admin.Main.app')
@section('app')  
    <div class="container-fluid">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex justify-content-between">
                    <h4 class="page-title">EDITAR ESTRUTURA ADMINISTRATIVA</h4>
                </div>
            </div><!-- row -->
        </div><!-- page-breadcrumb --><br/>
        @if($errors->any()) 
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12">
                    <div class="alert alert-danger" role="alert">
                        <span>{{$errors->first()}}</span>
                    </div>
                </div>
            </div><!-- row -->
        @endif   
        <div class="row card">
            <div class="col-md-12">                
                @foreach($estruturaadministrativa as $ea)            
                        <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/estruturaadministrativa/update', $ea->id)}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="form-group">
                                    <label>NOME:</label>
                                    <input type="text" name="nome" class="form-control" value="{{$ea->nome}}">
                                </div>                                
                                <div class="form-group">                                    
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text" for="inputGroupSelect01">UNIDADES ADMINISTRATIVAS</label>
                                        </div>
                                        <select class="custom-select custom-select" name="unidades_administrativas">
                                        @if(!$ea->unidades_administrativas)
                                            <option value="0" selected>NÃO</option>                                        
                                            <option value="1">SIM</option>
                                        @else
                                            <option value="0">NÃO</option>                                        
                                            <option value="1" selected  >SIM</option>
                                        @endif                                                                    
                                    </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>RESPONSÁVEL:</label>
                                    <input type="text" name="responsavel" class="form-control" value="{{$ea->responsavel}}">
                                </div>
                                <div class="form-group">
                                    <label>ENDEREÇO:</label>
                                    <input type="text" name="endereco" class="form-control" value="{{$ea->endereco}}">
                                </div>
                                <div class="form-group">
                                    <label>CEP:</label>
                                    <input type="text" name="cep" class="form-control" value="{{$ea->cep}}">
                                </div>
                                <div class="form-group">
                                    <label>FUNCIONAMENTO:</label>
                                    <input type="text" name="hr_funcionamento" class="form-control" value="{{$ea->hr_funcionamento}}">
                                </div>
                                <div class="form-group">
                                    <label>TELEFONE:</label>
                                    <input type="text" name="telefone" class="form-control" value="{{$ea->telefone}}">
                                </div>
                                <div class="form-group">
                                    <label>E-MAIL:</label>
                                    <input type="text" name="email" class="form-control" value="{{$ea->email}}">
                                </div>
                                <div class="form-group">
                                    <label>IMAGEM:</label>
                                    <input type="file" name="url_imagem" class="form-control" value="{{$ea->url_imagem}}">
                                </div>
                                <div class="form-group">
                                    <label>COMPETÊNCIAS:</label>
                                    <textarea class="form-control" name="competencias" rows="6">{{$ea->competencias}}</textarea>
                                </div>                                                            
                                <div class="box-footer d-flex justify-content-between">
                                    <button type="submit" class="btn btn-success pull-right">
                                        <i class="fas fa-wrench"></i>
                                        <span>ATUALIZAR</span>
                                    </button>
                                    <a href="/phpmyadmin/restrito/estruturaadministrativa" class="btn btn-secondary" data-dismiss="modal">
                                        <i class="far fa-window-close"></i>
                                        <sapn>CANCELAR</sapn>
                                    </a>
                                </div>
                            </form> 
                
                @endforeach
            </div><!-- col-md-12 -->        
        </div><!-- row -->
    </div><!-- container-fluid -->
@stop