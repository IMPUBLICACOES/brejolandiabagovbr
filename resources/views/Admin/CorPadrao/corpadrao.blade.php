@extends('Admin.Main.app')
@section('app')  
    <div class="container-fluid">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex justify-content-between">
                    <h4 class="page-title">COR PADRÃO</h4>                    
                    <a type="submit" class="btn btn-warning pull-right" data-toggle="modal" data-target="#modalCor">
                        <i class="fas fa-wrench"></i>
                        <span>EDITAR</span>
                    </a>                    
                </div>
            </div><!-- row -->
        </div><!-- page-breadcrumb --><br/> 

        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="modalCor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content card">                    
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>                                
                        </div>                        
                        <div class="card-body">                            
                            @foreach ($cor as $c)
                            <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/corpadrao/update', $c->id)}}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="form-group">                                    
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">HEXADECIMAL</span>
                                            <span class="input-group-text">INSIRA O '#'</span>
                                        </div>
                                        <input type="text" name="cor_hexa" class="form-control" value="{{$c->cor_hexa}}">
                                        
                                    </div>
                                </div>                                
                                <div class="box-footer d-flex justify-content-between">
                                    <button type="submit" class="btn btn-success pull-right">
                                        <i class="fas fa-plus-square"></i>
                                        <span>ATUALIZAR</span>
                                    </button>
                                    <div></div>                                  
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                        <i class="far fa-window-close"></i>
                                        <span>CANCELAR</span>
                                    </button>
                                </div>                                
                            </form>
                            @endforeach
                        </div>                    
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </div><!-- modal fade -->
        </div><!-- row -->        
    

        @if($errors->any())                                
            <div class="row">                
                <div class="col-xs-12 col-md-12 col-lg-12">                
                    <div class="alert alert-danger" role="alert">                                                    
                        <span>{{$errors->first()}}</span>
                    </div>                        
                </div>
            </div><!-- row -->            
        @endif

                 
        @if($cor)        
            <div class="row">                
                <div class="col-xs-12 col-md-12 col-lg-12 text-center">
                    @foreach ($cor as $c)
                        <div class="alert" style="background-color:{{$c->cor_hexa}}" role="alert">
                            <span class="text-light h1">COR PADRÃO DO SITE</span>
                        </div>                                            
                    @endforeach
                </div>
            </div><!-- row -->            
        @else
            <div class="row">                
                <div class="col-xs-12 col-md-12 col-lg-12 text-center">                    
                    <div class="alert bg-warning" role="alert">
                        <span class="text-light h1">CADASTRE UM COR PADRÃO PARA SEU SITE</span>
                    </div>                                                                
                </div>
            </div><!-- row -->   
        @endif    
    </div><!-- container-fluid -->
@stop