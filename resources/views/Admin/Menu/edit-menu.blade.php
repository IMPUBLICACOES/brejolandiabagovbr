@extends('Admin.Main.app')
@section('app')  
    <div class="container-fluid">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex justify-content-between">
                    <h4 class="page-title">EDITAR MENU</h4>                                                    
                </div>
            </div><!-- row -->
        </div><!-- page-breadcrumb --><br/>              

        <div class="row card">
            <div class="col-md-12">                
                @if($errors->any())                                 
                    <div class="col-xs-12 col-md-12 col-lg-12">                
                        <div class="alert alert-danger" role="alert">                                                    
                            <span>{{$errors->first()}}</span>
                        </div>                        
                    </div>            
                @endif
                <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/menu/update', $menu->id)}}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="form-group">
                        <label>NOME:</label>
                        <input type="text" name="nome" class="form-control" value="{{$menu->nome}}">
                    </div>     
                    <div class="form-group">
                        <label>LINK:</label>
                        <input type="text" name="link" class="form-control" value="{{$menu->link}}">
                    </div>                               
                    <div class="form-group">                            
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">SUB-MENU</label>
                            </div>
                            <select class="custom-select custom-select" name="sub_menu">                                        
                                @if (!$menu->sub_menu)
                                    <option value="0" selected>NÃO</option>
                                    <option value="1">SIM</option>
                                    @else
                                    <option value="1" selected>SIM</option>
                                    <option value="0">NÃO</option>
                                @endif                                
                            </select>
                        </div>   
                    </div>
                    <div class="box-footer d-flex justify-content-between">
                        <button type="submit" class="btn btn-success pull-right">
                            <i class="fas fa-wrench"></i>
                            <span>ATUALIZAR</span>
                        </button>
                        <a href="/phpmyadmin/restrito/menu" class="btn btn-secondary" data-dismiss="modal">
                            <i class="far fa-window-close"></i>
                            <sapn>CANCELAR</sapn>
                        </a>
                    </div>                           
                </form> 
            </div><!-- row -->        
        </div><!-- col-md-12 -->
    </div><!-- container-fluid -->    
@stop