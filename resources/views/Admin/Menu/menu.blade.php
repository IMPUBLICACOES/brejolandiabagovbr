@extends('Admin.Main.app')
@section('app')  
    <div class="container-fluid">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex justify-content-between">
                    <h4 class="page-title">MENUS E SUB-MENUS</h4>                                   
                </div>
            </div><!-- row -->
        </div><!-- page-breadcrumb --><br/> 
           
        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="modalMenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content card">                    
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>                                
                        </div>                        
                        <div class="card-body">                            
                            <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/menu/create')}}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="form-group">
                                    <label>NOME:</label>
                                    <input type="text" name="nome" class="form-control">
                                </div>
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text" for="inputGroupSelect01">SUB-MENU</label>
                                        </div>
                                        <select class="custom-select custom-select" name="sub_menu">
                                            <option value="0" selected>NÃO</option>
                                            <option value="1">SIM</option>                                        
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>LINK:</label>
                                    <input type="text" name="link" class="form-control">
                                </div>                               
                                <div class="box-footer d-flex justify-content-between">
                                    <button type="submit" class="btn btn-success pull-right">
                                        <i class="fas fa-plus-square"></i>
                                        <span>CRIAR</span>
                                    </button>
                                    <div></div>                                  
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                        <i class="far fa-window-close"></i>
                                        <span>CANCELAR</span>
                                    </button>
                                </div>                                
                            </form>                            
                        </div>                    
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </div><!-- modal fade -->
        </div><!-- row -->        
    
       
        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="modalSubMenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content card">                    
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>                                
                        </div>                        
                        <div class="card-body">                            
                            <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/submenu/create')}}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="form-group">
                                    <label>NOME:</label>
                                    <input type="text" name="nome" class="form-control">
                                </div>
                                <div class="form-group">                                    
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text" for="inputGroupSelect01">MENU</label>
                                        </div>
                                        <select class="custom-select custom-select" name="id_menu">                                        
                                            <option selected>SELECIONE O MENU</option>
                                            @foreach ($menus as $m)
                                                @if($m->sub_menu)
                                                    <option value="{{$m->id}}">{{$m->nome}}</option>                                            
                                                @endif                                        
                                            @endforeach                                                                      
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>LINK:</label>
                                    <input type="text" name="link" class="form-control">
                                </div>                               
                                <div class="box-footer d-flex justify-content-between">
                                    <button type="submit" class="btn btn-success pull-right">
                                        <i class="fas fa-plus-square"></i>
                                        <span>CRIAR</span>
                                    </button>
                                    <div></div>                                  
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                        <i class="far fa-window-close"></i>
                                        <span>CANCELAR</span>
                                    </button>
                                </div>                                
                            </form>                            
                        </div>                    
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </div><!-- modal fade -->
        </div><!-- row -->        
    

    
            <div class="col-xs-12 col-md-12 col-lg-12">                
                @if($errors->any())                        
                <div class="alert alert-danger" role="alert">                                                    
                    <span>{{$errors->first()}}</span>
                </div>                        
                @endif                    
            </div>
            



            <div class="row card">
                <div class="col-12 d-flex justify-content-between">
                    <div></div><!-- row -->
                    <h4 class="page-title h3">MENUS</h4>                    
                    <a type="submit" class="btn btn-success pull-right" data-toggle="modal" data-target="#modalMenu">
                        <i class="fas fa-plus-square"></i>
                        <span>NOVO MENU</span>
                    </a>
                </div><br>                
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th scope="col">NOME</th>
                            <th scope="col">SUB-MENU</th>
                            <th scope="col">LINK</th>                                                                                          
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($menus as $m)
                        <tr>
                            <td scope="col">{{$m->nome}}</td>
                            <td scope="col">{{$m->link}}</td>
                            <td scope="col">
                                <span class="badge badge-pill badge-info">
                                    @if ($m->sub_menu)
                                        SIM<br>
                                    @else                                
                                        NÃO<br>
                                    @endif
                                </span>
                            </td>                                                                                                                                                      
                            <td scope="col">
                                <form action="{{URL::to('/phpmyadmin/restrito/menu/excluir', $m->id )}}" method="GET" enctype="multipart/form-data">
                                    <button type="submit" class="btn btn-danger pull-rightborder border-secondary text-dark">
                                        <i class="fas fa-trash-alt"></i>
                                        <span> EXCLUIR </span>                        
                                    </button>                        
                                </form>
                            </td>
                            <td scope="col">
                                <a type="submit" href="{{URL::to('/phpmyadmin/restrito/menu/edit', $m->id )}}" class="btn btn-warning pull-right text-dark">
                                    <i class="fas fa-edit"></i>
                                    <span> EDITAR </span>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>                
            </div><!-- row -->
  
            <div class="row card">
                <div class="col-12 d-flex justify-content-between">
                    <div></div><!-- row -->
                    <h4 class="page-title h3">SUB-MENUS</h4>                    
                    <a type="submit" class="btn btn-success pull-right" data-toggle="modal" data-target="#modalSubMenu">
                        <i class="fas fa-plus-square"></i>
                        <span>NOVO SUB-MENU</span>
                    </a>
                </div><br>
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th scope="col">NOME</th>                            
                            <th scope="col">LINK</th>                                                                                          
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($sub as $s)
                            <tr>
                                <td scope="col">{{$s->nome}}</td>                            
                                <td scope="col">{{$s->link}}</td>
                                <td scope="col">                            
                                    <form action="{{URL::to('/phpmyadmin/restrito/submenu/excluir', $s->id )}}" method="GET" enctype="multipart/form-data">
                                        <button type="submit" class="btn btn-danger pull-right border border-secondary text-dark">
                                            <i class="fas fa-trash-alt"></i>
                                            <span> EXCLUIR </span>                        
                                        </button>                        
                                    </form>
                                </td>
                                <td scope="col">
                                    <a type="submit" href="{{URL::to('/phpmyadmin/restrito/submenu/edit', $s->id )}}" class="btn btn-warning pull-right text-dark" >
                                        <i class="fas fa-edit"></i>
                                        <span> EDITAR </span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- col-md-12 -->
        </div><!-- row -->
    </div><!-- container-fluid -->    
@stop