@extends('Admin.Main.app')
@section('app')  
    <div class="container-fluid">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-12 d-flex justify-content-between">
                    <h4 class="page-title">FACEBOOK</h4>                    
                    <a type="submit" class="btn btn-warning pull-right" data-toggle="modal" data-target="#modalFace">
                        <i class="fas fa-wrench"></i>
                        <span>EDITAR</span>
                    </a>                    
                </div>
            </div><!-- row -->
        </div><!-- page-breadcrumb --><br/> 

        <div class="row">
            <!-- Modal -->
            <div class="modal fade" id="modalFace" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content card">                    
                        <div class="d-flex justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                                <span aria-hidden="true">&times;</span>
                            </button>                                
                        </div>                        
                        <div class="card-body">                            
                            <div class="card-body d-flex flex-column align-items-start">                                    
                                <p class="card-text mb-auto">INSIRA A URL DO FACEBOOK DESEJADO</p>                    
                                <p class="card-text mb-auto">EX: https://www.facebook.com/facebook</p>                                        
                                <p class="card-text mb-auto">APENAS O 'facebook' APÓS A '/' É NECESSÁRIO</p>                    
                            </div>                            
                            @foreach ($face as $f)                                       
                            <form class="thumbnail" action="{{URL::to('/phpmyadmin/restrito/facebook/update', $f->id)}}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <div class="form-group">                                                                        
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">https://www.facebook.com</span>
                                            <span class="input-group-text">/</span>
                                        </div>
                                        <input type="text" name="url" class="form-control" value="{{$f->url}}">                                        
                                    </div>                                 
                                </div>                  
                                <div class="box-footer d-flex justify-content-between">
                                    <button type="submit" class="btn btn-success pull-right">
                                        <i class="fas fa-wrench"></i>
                                        <span>ATUALIZAR</span>
                                    </button>
                                    <a href="/phpmyadmin/restrito/noticias"class="btn btn-secondary" data-dismiss="modal">
                                        <i class="far fa-window-close"></i>
                                        <sapn>CANCELAR</sapn>
                                    </a>
                                </div>                                
                            </form>               
                            @endforeach                       
                        </div>                    
                    </div><!-- modal-content -->
                </div><!-- modal-dialog -->
            </div><!-- modal fade -->
        </div><!-- row -->           
    
        @if($errors->any())                        
            <div class="col-xs-12 col-md-12 col-lg-12">                
                <div class="alert alert-danger" role="alert">                                                    
                    <span>{{$errors->first()}}</span>
                </div>                        
            </div>
        @endif                    
            
        <div class="row card">    
            <div class="flex-md-row mb-4 shadow-sm h-md-250">
                <div class="card-body d-flex flex-column align-items-start">
                    <strong class="d-inline-block mb-2 text-success">URL CADASTRADA</strong>
                    <h3 class="mb-0">
                        <p class="text-dark" href="#">{{$f->url}}</p>
                    </h3><br>
                    <p class="card-text mb-auto">INSIRA A URL DO FACEBOOK DESEJADO</p>                    
                    <p class="card-text mb-auto">EX: https://www.facebook.com/facebook</p>                                        
                    <p class="card-text mb-auto">APENAS O 'facebook' APÓS A '/' É NECESSÁRIO</p>                    
                </div>                
            </div>   
        </div><!-- row -->        
    </div><!-- container-fluid -->                
@stop