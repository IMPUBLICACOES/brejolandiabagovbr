require('./bootstrap');

window.Vue = require('vue');
window.Axios = require('axios').default;
window.VueAxios = require('vue-axios').default;

Vue.component('aviso-1-component', require('./components/Site/Aviso/AvisoComponent.vue').default);
// - NAVBAR -- MENUS
Vue.component('navbar-1-component', require('./components/Site/NavBar/Navbar1Component.vue').default);
Vue.component('navbar-2-component', require('./components/Site/NavBar/Navbar2Component.vue').default);
Vue.component('navbar-3-component', require('./components/Site/NavBar/Navbar3Component.vue').default);
Vue.component('navbar-4-component', require('./components/Site/NavBar/Navbar4Component.vue').default);
Vue.component('navbar-5-component', require('./components/Site/NavBar/Navbar5Component.vue').default);
Vue.component('navbar-6-component', require('./components/Site/NavBar/Navbar6Component.vue').default);
Vue.component('navbar-7-component', require('./components/Site/NavBar/Navbar7Component.vue').default);
Vue.component('navbar-8-component', require('./components/Site/NavBar/Navbar8Component.vue').default);
Vue.component('navbar-9-component', require('./components/Site/NavBar/Navbar9Component.vue').default);
// - NAVBAR -- MENUS

// - HEAD -- CABEÇALHO
Vue.component('head-1-component', require('./components/Site/Head/Head1Component.vue').default);
// - HEAD -- CABEÇALHO

// - FOOTER -- RODAPÉ
Vue.component('footer-1-component', require('./components/Site/Footer/Footer1Component.vue').default);
Vue.component('footer-2-component', require('./components/Site/Footer/Footer2Component.vue').default);
Vue.component('footer-3-component', require('./components/Site/Footer/Footer3Component.vue').default);
Vue.component('footer-4-component', require('./components/Site/Footer/Footer4Component.vue').default);
Vue.component('footer-5-component', require('./components/Site/Footer/Footer5Component.vue').default);
Vue.component('footer-6-component', require('./components/Site/Footer/Footer6Component.vue').default);
Vue.component('footer-7-component', require('./components/Site/Footer/Footer7Component.vue').default);
// - FOOTER -- RODAPÉ

// - GALLERY -- GALERIA
Vue.component('galeria-1-component', require('./components/Site/Galeria/Galeria1Component.vue').default);
Vue.component('galeria-2-component', require('./components/Site/Galeria/Galeria2Component.vue').default);
Vue.component('galeria-3-component', require('./components/Site/Galeria/Galeria3Component.vue').default);
Vue.component('galeria-4-component', require('./components/Site/Galeria/Galeria4Component.vue').default);
Vue.component('galeria-5-component', require('./components/Site/Galeria/Galeria5Component.vue').default);
Vue.component('galeria-6-component', require('./components/Site/Galeria/Galeria6Component.vue').default);
// - GALLERY -- GALERIA

// - BANNER -- FOTOS
Vue.component('banner-1-component', require('./components/Site/Banner/Banner1Component.vue').default);
Vue.component('banner-2-component', require('./components/Site/Banner/Banner2Component.vue').default);
Vue.component('banner-3-component', require('./components/Site/Banner/Banner3Component.vue').default);
Vue.component('banner-4-component', require('./components/Site/Banner/Banner4Component.vue').default);
Vue.component('banner-5-component', require('./components/Site/Banner/Banner5Component.vue').default);
// - BANNER -- FOTOS

// - NEWS -- NOTÍCIAS
Vue.component('noticia-1-component', require('./components/Site/Noticia/Noticia1Component.vue').default);
Vue.component('noticia-2-component', require('./components/Site/Noticia/Noticia2Component.vue').default);
Vue.component('noticia-3-component', require('./components/Site/Noticia/Noticia3Component.vue').default);
Vue.component('noticia-4-component', require('./components/Site/Noticia/Noticia4Component.vue').default);
Vue.component('noticia-5-component', require('./components/Site/Noticia/Noticia5Component.vue').default);
Vue.component('noticia-6-component', require('./components/Site/Noticia/Noticia6Component.vue').default);
// - NEWS -- NOTÍCIAS

// - TEMP -- PREVISÃO
Vue.component('previsao-1-component', require('./components/Site/Previsao/Previsao1Component.vue').default);
Vue.component('previsao-2-component', require('./components/Site/Previsao/Previsao2Component.vue').default);
Vue.component('previsao-3-component', require('./components/Site/Previsao/Previsao3Component.vue').default);
// - TEMP -- PREVISÃO

// - MAPS -- MAPAS
Vue.component('maps-1-component', require('./components/Site/Maps/Maps1Component.vue').default);
// - MAPS -- MAPAS

// - CARD -- CARTÃO
Vue.component('card-1-component', require('./components/Site/Card/Card1Component.vue').default);
Vue.component('card-2-component', require('./components/Site/Card/Card2Component.vue').default);
Vue.component('card-3-component', require('./components/Site/Card/Card3Component.vue').default);
Vue.component('card-4-component', require('./components/Site/Card/Card4Component.vue').default);
Vue.component('card-5-component', require('./components/Site/Card/Card5Component.vue').default);
Vue.component('card-midias-component', require('./components/Site/Card/CardMidiasComponent.vue').default);
Vue.component('card-gestao-component', require('./components/Site/Card/CardGestaoComponent.vue').default);
Vue.component('card-diario-component', require('./components/Site/Card/CardDiarioComponent.vue').default);
Vue.component('card-municipio-component', require('./components/Site/Card/CardMunicipioComponent.vue').default);
Vue.component('card-secretarias-component', require('./components/Site/Card/CardSecretariasComponent.vue').default);

Vue.component('transparencia-component', require('./components/Site/Card/TransparenciaComponent.vue').default);

// - CARD -- CARTÃO

// - ADMIN -- ADMINISTRAÇÃO

// - ADMIN -- ADMINISTRAÇÃO

const app = new Vue({
    el: '#app',
});
